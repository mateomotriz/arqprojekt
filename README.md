# arqProjekt

![alt text](https://gitlab.com/mateomotriz/arqprojekt/badges/master/pipeline.svg "pipeline badge")
![alt text](https://img.shields.io/badge/lint-flake8-blueviolet "lint badge")


<p align="center">
  <img height="240" src="https://cinemagavia.es/wp-content/uploads/2020/01/El-faro.jpg">
</p>


**development**: python + vscode

**all credits**: @mateomotriz


