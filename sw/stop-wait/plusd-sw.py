# BEISPIEL: python3 plusd-sw.py ../.rxarq/mateo.txt 1234

import socket  # sockets
import sys  # use arguments in command line

# server
IP_SERVER = "127.0.0.14"
DATA_SIZE = 4096

# client
NSEQ_INIT = b'\x00\x00\x00'
MAX_NSEQ = b'\xFF\xFF\xFF'
BYTES_NSEQ = 3


def writeFile(file_x, file_array_x):
    # print("array: ", file_array_x)

    f = open(file_x, 'w')
    f.write("")             # clean the file
    f.close()

    f = open(file_x, 'ab')
    for i in file_array_x:
        f.write(i)  # write at the end of the file
    f.close()


def plusd(ip_client_x, port_x, nseq_server_x, file_array_x):
    # communication nodes
    data_client, ip_shuffle = server.recvfrom(DATA_SIZE)  # size of the data
    ip_client = data_client[0:4]
    port_client = data_client[4:6]
    bytes_header = ip_client + port_client

    # check data integrity
    last_byte_x = False
    nseq_data_x = data_client[6:(6+BYTES_NSEQ)]

    if (nseq_data_x == nseq_server_x):
        # data valid, it's the first time we receive it
        data_retrieved_x = data_client[(6+BYTES_NSEQ):]
        if (data_retrieved_x == b'end-mateo'):
            last_byte_x = True
            nseq_server_x = NSEQ_INIT

        else:
            # if they match we save the data and increase by 1 the nseq
            if (nseq_server_x == MAX_NSEQ):
                nseq_server_x = NSEQ_INIT
            else:
                nseq_server_x = (int.from_bytes(nseq_server_x, 'big') +
                                 1).to_bytes(BYTES_NSEQ, 'big')
            file_array_x.append(data_retrieved_x)

        # print("data: ", data_retrieved_x, "with nseq: ", nseq_data_x)

    # send ACK
    server.sendto(bytes_header + nseq_data_x, ip_shuffle)

    return last_byte_x, nseq_server_x, file_array_x


# MAIN:
if (len(sys.argv) != 3):
    print("wrong syntax! the structure must be the following one:")
    print("<<python3 plusd.py output_file port>>")
    sys.exit()

# ---VARIABLES
FILE_WRITE = str(sys.argv[1])
PORT_SERVER = sys.argv[2]

# ---EXECUTION
# AF_INET: ipv4 address, SOCK_DATAGRAM: udp protocol (STREAM 4 tcp)
server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# bind(address, port)
server.bind((str(IP_SERVER), int(PORT_SERVER)))

# prepare file
file_array = []
nseq_server = NSEQ_INIT

while True:
    last_byte, nseq_server, file_array = plusd(IP_SERVER, PORT_SERVER,
                                               nseq_server, file_array)

    if (last_byte):
        print("new file received&retrieved!")
        writeFile(FILE_WRITE, file_array)
        file_array = []
