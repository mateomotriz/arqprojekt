# BEISPIEL: python3 plus-sw.py ../.txarq/mateo.txt 127.0.0.14 1234 127.0.0.1 2019

import socket  # use sockets
import sys     # handle parameters
import math    # ops
import time    # measure times

# ---VARIABLES & CONSTANTS
# data
PACKAGE_LENGTH = 1470  # in bytes

# datagrams
DATA_SIZE = 4096
NSEQ_INIT = b'\x00\x00\x00'
MAX_NSEQ = b'\xFF\xFF\xFF'
BYTES_NSEQ = 3

# dynamic timer
default_timeout = 1  # s
first_rtx = True
alfa = 1 / 8
beta = 1 / 4
rtt_array = [0, 0]  # k, k-1
dev_array = [0, 0]  # k, k-1


def readFile(file_x):
    # open the file in read-binary mode
    f = open(file_x, "rb")
    data_file_x = f.read()
    f.close()

    # how many packages?
    package_number_x = math.ceil(len(data_file_x) / PACKAGE_LENGTH)
    print(package_number_x, "packages prepared to be sent")

    # fill vector with data
    data_array_x = []
    for x in range(0, len(data_file_x), PACKAGE_LENGTH):
        data_array_x.append(data_file_x[x:x + PACKAGE_LENGTH])
    data_array_x.append(b'end-mateo')

    return data_array_x, package_number_x


def prepareHeader(ip_x, port_x):
    bytes_ip_x = b''
    bytes_port_x = b''

    # split the ip in 4 bytes
    split_ip = ip_x.split('.')

    # prepare ip and port, in 4 and 2 bytes respectively
    bytes_port_x = (int(port_x)).to_bytes(2, 'big')

    for i in split_ip:
        x = (int(i)).to_bytes(1, 'big')
        bytes_ip_x = bytes_ip_x + x
    # print("ip server: ", bytes_server_x[0:4], "& port: ", bytes_port_x[0:2])

    return (bytes_ip_x + bytes_port_x)


def dynamicTimer(rtt_measured_x):
    global first_rtx
    if (first_rtx):
        rtt_array[0] = rtt_measured_x
        dev_array[0] = rtt_array[0] / 2
        first_rtx = False
    else:
        rtt_array[1] = rtt_array[0]  # k = k-1
        dev_array[1] = dev_array[0]  # k = k-1
        # recalculate the values
        rtt_array[0] = (1 - alfa) * rtt_array[1] + alfa * rtt_measured_x
        dev_array[0] = (1 - beta) * dev_array[1] + beta * \
            abs(rtt_array[0]-rtt_measured_x)

    RTO_x = rtt_array[0]+4*dev_array[0]
    # print("rtt: ", rtt_array, " and rto: ", RTO_x)
    return RTO_x


def plus(ip_server_x, port_x, datagram, nseq_client_x):
    time_start_ms = time.time_ns() / 10**6  # ms
    client.sendto(datagram, (str(ip_server_x), int(port_x)))

    try:
        data_server, addr_server = client.recvfrom(DATA_SIZE)

    except socket.timeout:
        return False, nseq_client_x

    # update dynamic timer
    time_end_ms = time.time_ns() / 10**6  # ms
    rtt_measured = time_end_ms - time_start_ms
    rto_timeout = dynamicTimer(rtt_measured)
    # print("rto: ", rto_timeout, " ms")
    client.settimeout(float(rto_timeout/1000))

    # split data and check nseq
    nseq_server_x = data_server[6:(6+BYTES_NSEQ)]
    # print ("nseq server: ", nseq_server_x)

    # check ACK!
    if (nseq_client_x == nseq_server_x):
        if (nseq_client_x == MAX_NSEQ):
            nseq_client_x = NSEQ_INIT
        else:
            nseq_client_x = (int.from_bytes(nseq_client_x, 'big') +
                             1).to_bytes(BYTES_NSEQ, 'big')
        return (True, nseq_client_x)
    else:
        # print("wops, shufflerouter is a naughty boy")
        return (False, nseq_client_x)


# **************************************************************************************************************************

# MAIN
if (len(sys.argv) != 6):
    print("sintaxis incorrecta, debe tener esta estructura")
    print(
        "<<python3 plus.py input_file dest_IP dest_port emulator_IP emulator_port>>"
    )
    sys.exit()

# command parameters
FILE_READ = str(sys.argv[1])
IP_SERVER = str(sys.argv[2])
PORT_SERVER = sys.argv[3]
IP_SHUFFLE = str(sys.argv[4])
PORT_SHUFFLE = sys.argv[5]

# ---EXECUTION
client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client.settimeout(float(default_timeout))

# prepare and encode data in bytes
bytes_header = prepareHeader(IP_SERVER, PORT_SERVER)
data_file, package_number = readFile(FILE_READ)

# send data
nseq_client = NSEQ_INIT

end_transmission = False
file_i = 0
time_total_init = time.time_ns() / 10**9

while not (end_transmission):
    # print("nseq: ", nseq_client)
    ack_match, nseq_client = plus(
        IP_SERVER, PORT_SHUFFLE,
        bytes_header + nseq_client + data_file[file_i], nseq_client)

    # cool percentage bar (but it consumes a lot of recurses):
    # print("sending...", round((file_i / len(data_file)) * 100, 2), " %")

    if (data_file[file_i] == b'end-mateo' and ack_match):
        time_total_end = time.time_ns() / 10**9
        print("total time: ", time_total_end-time_total_init, " s")
        # this is the end
        end_transmission = True
    elif (ack_match):
        file_i += 1

client.close()
