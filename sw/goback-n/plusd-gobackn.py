# BEISPIEL: python3 plusd-gobackn.py ../.rxarq/mateo.txt 1234

import socket
import sys

# server
IP_SERVER = "0.0.0.0"
DATA_SIZE = 4096

# client
NSEQ_INIT = b'\x00\x00\x00'
MAX_NSEQ = b'\xFF\xFF\xFF'
BYTES_NSEQ = 3
BYTES_TIMESTAMP = 5


def writeFile(file_x, file_array_x):
    f = open(file_x, 'w')
    f.write("")  # clean the file
    f.close()

    f = open(file_x, 'ab')
    for i in file_array_x:
        f.write(i)  # write at the end of the file
    f.close()


# **************************************************************************************************************************

# MAIN:
if (len(sys.argv) != 3):
    print("wrong syntax! the structure must be the following one:")
    print("<<python3 plusd.py output_file port>>")
    sys.exit()

# ---VARIABLES
FILE_WRITE = str(sys.argv[1])
PORT_SERVER = sys.argv[2]

# ---EXECUTION
# AF_INET: ipv4 address, SOCK_DATAGRAM: udp protocol (STREAM 4 tcp)
server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# bind(address, port)
server.bind((str(IP_SERVER), int(PORT_SERVER)))

# prepare file
file_array = []
nseq_server = NSEQ_INIT

while True:
    # communication nodes
    data_client, ip_shuffle = server.recvfrom(DATA_SIZE)  # size of the data
    ip_client = data_client[0:4]
    port_client = data_client[4:6]
    bytes_header = ip_client + port_client

    # check data integrity
    last_byte = False
    nseq_data = data_client[6:(6 + BYTES_NSEQ)]
    timestamp = data_client[(6 + BYTES_NSEQ):(6 + BYTES_NSEQ +
                                              BYTES_TIMESTAMP)]

    if (nseq_data == b''):
        nseq_server = NSEQ_INIT
        break
    else:
        # if they match we save the data and increase by 1 the nseq
        if (nseq_server == nseq_data):
            # save the data save the world
            data_retrieved = data_client[(6 + BYTES_NSEQ + BYTES_TIMESTAMP):]
            file_array.append(data_retrieved)
            # increased nseq counter by 1
            nseq_server = (int.from_bytes(nseq_server, 'big') + 1).to_bytes(
                BYTES_NSEQ, 'big')

        # send ACK
        server.sendto(bytes_header + nseq_server + timestamp, ip_shuffle)

# C'EST FINI!
print("new file received&retrieved!")
writeFile(FILE_WRITE, file_array)
file_array = []
