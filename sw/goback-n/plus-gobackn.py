# BEISPIEL: python3 plus-gobackn.py ../.txarq/mateo.txt 127.0.0.14 1234 127.0.0.1 2019 200

import socket
import sys
import math
import time
import select

# ---VARIABLES & CONSTANTS
# data
PACKAGE_LENGTH = 1460  # in bytes
total_packages = 0
TX_WINDOW_SIZE = 200

# datagrams
DATA_SIZE = 4096
NSEQ_INIT = b'\x00\x00\x00'
MAX_NSEQ = b'\xFF\xFF\xFF'
BYTES_NSEQ = 3
BYTES_TIMESTAMP = 5

# dynamic timer
default_timeout = 100
rto_timeout = 100  # s
first_rtx = True
alfa = 1 / 8
beta = 1 / 4
rtt_array = [0, 0]  # k, k-1
dev_array = [0, 0]  # k, k-1

# select
inputs = []
outputs = []
# data_queue = {} # in the future!

# c'est fini c'est la vie
end_transmission = False
MATEO = False


class Datagram:
    def __init__(self, datagram, nseq, acked, timestamp=None):
        self.datagram = datagram
        self.nseq = nseq
        self.acked = acked
        if timestamp is None:
            self.timestamp = b''
        else:
            self.timestamp = timestamp

    def updateTimestamp(self):
        # timestamp (not dynamic)
        timestamp_ms = (time.time() - time_ref) * 10e3  # ms
        timestamp_bytes = (round(timestamp_ms)).to_bytes(
            BYTES_TIMESTAMP, 'big')
        self.timestamp = timestamp_bytes

    def updateACK(self, acked):
        self.acked = acked


def readFile(file_x):
    # open the file in read-binary mode
    f = open(file_x, "rb")
    data_file_x = f.read()
    f.close()

    # how many packages?
    package_number_x = math.ceil(len(data_file_x) / PACKAGE_LENGTH)
    print(package_number_x, "packages prepared to be sent")

    # fill vector with data
    data_array_x = []
    i_x = 0
    for i in range(0, len(data_file_x), PACKAGE_LENGTH):
        data_array_x.append(
            Datagram(data_file_x[i:i + PACKAGE_LENGTH],
                     i_x.to_bytes(BYTES_NSEQ, 'big'), False))
        i_x += 1
    return data_array_x, package_number_x


def prepareHeader(ip_x, port_x):
    bytes_ip_x = b''
    bytes_port_x = b''

    # split the ip in 4 bytes
    split_ip = ip_x.split('.')

    # prepare ip and port, in 4 and 2 bytes respectively
    bytes_port_x = (int(port_x)).to_bytes(2, 'big')

    for i in split_ip:
        x = (int(i)).to_bytes(1, 'big')
        bytes_ip_x = bytes_ip_x + x

    return (bytes_ip_x + bytes_port_x)


def dynamicTimer(rtt_measured_x):
    global first_rtx
    if (first_rtx):
        rtt_array[0] = rtt_measured_x
        dev_array[0] = rtt_array[0] / 2
        first_rtx = False
    else:
        rtt_array[1] = rtt_array[0]  # k = k-1
        dev_array[1] = dev_array[0]  # k = k-1
        # recalculate the values
        rtt_array[0] = (1 - alfa) * rtt_array[1] + alfa * rtt_measured_x
        dev_array[0] = (1 - beta) * dev_array[1] + beta * \
            abs(rtt_array[0]-rtt_measured_x)

    RTO_x = rtt_array[0] + 4 * dev_array[0]
    return RTO_x


# **************************************************************************************************************************

# MAIN
if len(sys.argv) == 7:
    TX_WINDOW_SIZE = int(sys.argv[6])
elif (len(sys.argv) != 6):
    print("sintaxis incorrecta, debe tener esta estructura")
    print(
        "<<python3 plus.py input_file dest_IP dest_port emulator_IP emulator_port>>"
    )
    sys.exit()

# command parameters
FILE_READ = str(sys.argv[1])
IP_SERVER = str(sys.argv[2])
PORT_SERVER = sys.argv[3]
IP_SHUFFLE = str(sys.argv[4])
PORT_SHUFFLE = sys.argv[5]

# ---EXECUTION
client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client.setblocking(0)
inputs.append(client)

# prepare and encode data in bytes
bytes_header = prepareHeader(IP_SERVER, PORT_SERVER)
data_file, package_number = readFile(FILE_READ)

# set refs
time_ref = time.time()  # s
nseq_client = NSEQ_INIT

# initial window
tx_window_tail = 0
tx_window_top = TX_WINDOW_SIZE - 1
tx_window_max = package_number

while not (end_transmission):
    # update the window top
    tx_window_top = tx_window_tail + TX_WINDOW_SIZE
    if tx_window_top > tx_window_max:
        tx_window_top = tx_window_max

    # default timeouts
    timeout_min_ms = None
    timeout_select = rto_timeout

    # send window
    for i in range(tx_window_tail, tx_window_top):
        # timeout if: time_package + rto_timeout < time_now
        timeout_ms = int.from_bytes(data_file[i].timestamp,
                                    'big') + rto_timeout
        time_now = (time.time() - time_ref) * 10e3
        if (data_file[i].timestamp == b'' or time_now > timeout_ms):
            # set timer and send
            data_file[i].updateTimestamp()
            total_packages += 1
            client.sendto(
                bytes_header + data_file[i].nseq + data_file[i].timestamp +
                data_file[i].datagram, (str(IP_SHUFFLE), int(PORT_SHUFFLE)))
        elif time_now < timeout_ms:
            if timeout_min_ms is None:
                timeout_min_ms = timeout_ms
                timeout_select = timeout_min_ms - (time.time() -
                                                   time_ref) * 10e3
            elif timeout_ms < timeout_min_ms:
                timeout_min_ms = timeout_ms
                timeout_select = timeout_min_ms - (time.time() -
                                                   time_ref) * 10e3

    if timeout_select < 0:
        timeout_select = 0
    # blocking funct: checks 4 new inputs
    readable, writable, exceptional = select.select(
        inputs, outputs, inputs, float(timeout_select / 10e3))

    if (readable):
        data_server, addr_server = client.recvfrom(DATA_SIZE)

        if TX_WINDOW_SIZE > 50:
            # update dynamic timer
            time_end_ms = (time.time() - time_ref) * 10e3
            timestamp_ms = data_server[(6 + BYTES_NSEQ):(6 + BYTES_NSEQ +
                                                         BYTES_TIMESTAMP)]
            time_start_ms = int.from_bytes(timestamp_ms, 'big')  # ms
            rtt_measured = time_end_ms - time_start_ms
            # "dynamic timer isn't worth with small windows" ~ mateo
            rto_timeout = dynamicTimer(rtt_measured)

        # split data and check nseq
        nseq_server = data_server[6:(6 + BYTES_NSEQ)]
        nseq_server_int = int.from_bytes(nseq_server, 'big')

        # last byte?
        if tx_window_tail < nseq_server_int:
            tx_window_tail = nseq_server_int
        if tx_window_tail == tx_window_max:
            end_transmission = True

# this is the end
for i in range(0, 5):
    client.sendto(bytes_header + b'', (str(IP_SHUFFLE), int(PORT_SHUFFLE)))

print("total time: ", time.time() - time_ref, " s")
print("total bytes: ",
      total_packages * (PACKAGE_LENGTH + BYTES_NSEQ + BYTES_TIMESTAMP))
client.close()
