import time

# dynamic timer
alfa = 1 / 8
beta = 1 / 4
rtt_array = [0, 0]  # k, k-1
dev_array = [0, 0]  # k, k-1
first_rtx = True


def dynamicTimer(rtt_measured_x):
    # it needs to be treated as a global variable (not a function local one)
    global first_rtx
    if (first_rtx):
        rtt_array[0] = rtt_measured_x
        dev_array[0] = rtt_array[0] / 2
        first_rtx = False
    else:
        rtt_array[1] = rtt_array[0] # k = k-1
        dev_array[1] = dev_array[0] # k = k-1
        # recalculate the values
        rtt_array[0] = (1 - alfa) * rtt_array[1] + alfa * rtt_measured_x
        dev_array[0] = (1 - beta) * dev_array[1] + beta * abs(rtt_array[0]-rtt_measured_x)

    RTO_x = rtt_array[0]+4*dev_array[0]
    print("rtt: ", rtt_array, " and rto: ", RTO_x)
    return RTO_x


# ---MAIN
dynamicTimer(10)
dynamicTimer(10)
dynamicTimer(10)
dynamicTimer(10)
dynamicTimer(10)
dynamicTimer(10)
dynamicTimer(10)
dynamicTimer(10)
dynamicTimer(10)
dynamicTimer(10)
dynamicTimer(10)
